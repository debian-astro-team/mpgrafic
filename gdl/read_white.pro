;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;   Copyright (C) 2008-2016 by Simon Prunet                               ;
;   prunet iap.fr                                                         ;
;                                                                         ;
;   This program is free software; you can redistribute it and/or modify  ;
;   it under the terms of the GNU General Public License as published by  ;
;   the Free Software Foundation; either version 2 of the License, or     ;
;   (at your option) any later version.                                   ;
;                                                                         ;
;   This program is distributed in the hope that it will be useful,       ;
;   but WITHOUT ANY WARRANTY; without even the implied warranty of        ;
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;
;   GNU General Public License for more details.                          ;
;                                                                         ;
;   You should have received a copy of the GNU General Public License     ;
;   along with this program; if not, write to the                         ;
;   Free Software Foundation, Inc.,                                       ;
;   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA              ;
;   or see https://www.gnu.org/licenses/licenses.html#GPL .               ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

pro read_white,filename,head,cube

openr,1,filename,/f77
head=lonarr(4)
readu,1,head
nx=head(0)
ny=head(1)
nz=head(2)

a=fltarr(nx,ny)
cube=fltarr(nx,ny,nz)
for i=0,nz-1 do begin
    readu,1,a
    cube(*,*,i)=a
endfor

close,1
end
