/* fftw3d_mpi_create_plan_c_wrap
 *
 * Copyright (c) 1997-1999, 2003 Massachusetts Institute of Technology
 * Copyright (c) 2017 Boud Roukema
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

/*
 This is a C wrapper for rfftw3d_f77_mpi_create_plan() to bypass the
 architecture-dependent behaviour of fftw2/openmpi described in
 Debian openmpi bug #851918. It is derived from mpi/rfftw_f77_mpi.c
 and mpi/fftw_f77_mpi.h in fftw-2.1.5-4.1 under GPL-2+. This requires
 `use iso_C_binding' in the calling program, such as mpgrafic-0.3.10.
 */

#include <rfftw_mpi.h>

void rfftw3d_f77_mpi_create_plan_c_wrap
(rfftwnd_mpi_plan *p, void *comm,
 int *nx, int *ny, int *nz, int *idir, int *flags);


/************************************************************************/
void rfftw3d_f77_mpi_create_plan_c_wrap
(rfftwnd_mpi_plan *p, void *comm,
 int *nx, int *ny, int *nz, int *idir, int *flags)
{
  fftw_direction dir = *idir < 0 ? FFTW_FORWARD : FFTW_BACKWARD;

  /* TODO: In principle, the first parameter should be `(MPI_Comm)comm',
     but there is an MPI (openmpi?) bug/feature related to the creation
     of duplicate communicators - MPI_Comm_dup. As long as mpgrafic remains
     a small program, the present hack of hardwiring MPI_COMM_WORLD should
     work. Strict compiler options will warn about *comm not being used. */
  *p = rfftw3d_mpi_create_plan(MPI_COMM_WORLD,
                              *nz,*ny,*nx,dir,*flags);
}
/****************************************************************************/
